package com.arjaywalter.openweather

import kotlin.math.round
import kotlin.math.roundToInt

fun String.toIconUrl(): String {
    return "http://openweathermap.org/img/w/$this.png"
}

fun Double.toDegreeCelsius(): String {
    return ((this - 273.15).roundToInt()).toString()
}