package com.arjaywalter.openweather.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.arjaywalter.openweather.data.WeatherRepository
import com.arjaywalter.openweather.model.WeatherItem

class WeatherDetailViewModel(
    private val repository: WeatherRepository,
    val id: Int
) : ViewModel() {

    private lateinit var weather: MutableLiveData<WeatherItem>

    fun getWeather(): LiveData<WeatherItem> {
        if (!::weather.isInitialized) {
            weather = MutableLiveData()
            loadWeather()
        }
        return weather
    }

    fun loadWeather() {
        weather = repository.fetchWeather(id)
    }

    internal class Factory(
        val repository: WeatherRepository,
        val id: Int
    ) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(WeatherDetailViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return WeatherDetailViewModel(repository, id) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }

    }

}
