package com.arjaywalter.openweather.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import com.arjaywalter.openweather.data.WeatherDataFactory
import com.arjaywalter.openweather.data.WeatherRepository
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.utils.NetworkState
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class WeatherListViewModel(
    private val repository: WeatherRepository,
    private val dataFactory: WeatherDataFactory
) : ViewModel() {

    private val executor: Executor = Executors.newFixedThreadPool(5)
    var networkState: LiveData<NetworkState>? = null
    var weathers: LiveData<PagedList<WeatherItem>>? = null

    init {
        networkState = Transformations.switchMap(dataFactory.mutableLiveData)
        { dataSource -> dataSource.networkState }

        val pagedListConfig = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(10)
            .setPageSize(20)
            .setPrefetchDistance(60)
            .build()

        weathers = LivePagedListBuilder(dataFactory, pagedListConfig)
            .setFetchExecutor(executor)
            .build()

    }

    fun refreshData() {
        //TODO refresh data from API
        //weathers = repository.fetchWeathers().data
    }

}
