package com.arjaywalter.openweather.ui

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import com.arjaywalter.openweather.R
import kotlinx.android.synthetic.main.activity_weather_detail.*

/**
 * An activity representing a single Weather detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a [WeatherListActivity].
 */
class WeatherDetailActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_detail)
        setSupportActionBar(detail_toolbar)


        // Show the Up button in the action bar.
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            val fragment = WeatherDetailFragment().apply {
                arguments = Bundle().apply {
                    putInt(
                        WeatherDetailFragment.ARG_ITEM_ID,
                        intent.getIntExtra(WeatherDetailFragment.ARG_ITEM_ID, 0)
                    )
                    putString(
                        WeatherDetailFragment.ARG_ITEM_NAME,
                        intent.getStringExtra(WeatherDetailFragment.ARG_ITEM_NAME)
                    )
                    putString(
                        WeatherDetailFragment.ARG_ITEM_TEMP,
                        intent.getStringExtra(WeatherDetailFragment.ARG_ITEM_TEMP)
                    )
                    putString(
                        WeatherDetailFragment.ARG_ITEM_MAIN,
                        intent.getStringExtra(WeatherDetailFragment.ARG_ITEM_MAIN)
                    )
                    putString(
                        WeatherDetailFragment.ARG_ITEM_ICON,
                        intent.getStringExtra(WeatherDetailFragment.ARG_ITEM_ICON)
                    )
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.weather_detail_container, fragment)
                .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.my_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                // This ID represents the Home or Up button. In the case of this
                // activity, the Up button is shown. For
                // more details, see the Navigation pattern on Android Design:
                //
                // http://developer.android.com/design/patterns/navigation.html#up-vs-back

                navigateUpTo(Intent(this, WeatherListActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
