package com.arjaywalter.openweather.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.arjaywalter.openweather.R

class SplashActivity : AppCompatActivity() {

    private val SPLASH_DELAY: Long = 2000.toLong()

    private val mHandler = Handler()
    private val mLauncher = Launcher()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_splash)

//        Timer().schedule(timerTask {
//            launch()
//        }, SPLASH_DELAY)

    }

    override fun onStart() {
        super.onStart()
        mHandler.postDelayed(mLauncher, SPLASH_DELAY)
    }

    override fun onStop() {
        mHandler.removeCallbacks(mLauncher)
        super.onStop()
    }

    private fun launch() {
        if (!isFinishing) {
            startActivity(Intent(applicationContext, WeatherListActivity::class.java))
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish()
        }
    }

    private inner class Launcher : Runnable {
        override fun run() {
            launch()
        }
    }

}
