package com.arjaywalter.openweather.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.arjaywalter.openweather.Injection
import com.arjaywalter.openweather.R
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.toDegreeCelsius
import com.arjaywalter.openweather.toIconUrl
import com.arjaywalter.openweather.ui.WeatherDetailFragment.Companion.ARG_ITEM_ICON
import com.arjaywalter.openweather.ui.WeatherDetailFragment.Companion.ARG_ITEM_ID
import com.arjaywalter.openweather.ui.WeatherDetailFragment.Companion.ARG_ITEM_MAIN
import com.arjaywalter.openweather.ui.WeatherDetailFragment.Companion.ARG_ITEM_NAME
import com.arjaywalter.openweather.ui.WeatherDetailFragment.Companion.ARG_ITEM_TEMP
import com.arjaywalter.openweather.utils.NetworkState
import kotlinx.android.synthetic.main.activity_weather_list.*
import kotlinx.android.synthetic.main.weather_list.*

/**
 * An activity representing a list of Pings. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a [WeatherDetailActivity] representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
class WeatherListActivity : AppCompatActivity(), ButtonFragment.OnFragmentInteractionListener {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private var twoPane: Boolean = false

    private lateinit var viewModel: WeatherListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        if (weather_detail_container != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true
        }
        // Get the ViewModel.
        viewModel = ViewModelProviders.of(this, Injection.provideViewModelFactory(this))
            .get(WeatherListViewModel::class.java)

        setupRecyclerView(weather_list)
    }

    private fun setupRecyclerView(recyclerView: RecyclerView) {
        val adapter = WeatherListAdapter { item: WeatherItem ->
            //Toast.makeText(this, weatherItem.name.toString(), Toast.LENGTH_SHORT).show()
            if (twoPane) {
                val fragment = WeatherDetailFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_ITEM_ID, item.id ?: 0)
                        putString(ARG_ITEM_NAME, item.name)
                        putString(ARG_ITEM_TEMP, item.main?.temp?.toDegreeCelsius())
                        item.weather?.let {
                            val weather = it[0]
                            putString(ARG_ITEM_ICON, weather.icon?.toIconUrl())
                            putString(ARG_ITEM_MAIN, weather.main)
                        }
                    }
                }
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.weather_detail_container, fragment)
                    .commit()
            } else {
                val intent = Intent(this, WeatherDetailActivity::class.java).apply {
                    putExtra(ARG_ITEM_ID, item.id)
                    putExtra(ARG_ITEM_NAME, item.name)
                    putExtra(ARG_ITEM_TEMP, item.main?.temp?.toDegreeCelsius())
                    item.weather?.let {
                        val weather = it[0]
                        putExtra(ARG_ITEM_ICON, weather.icon?.toIconUrl())
                        putExtra(ARG_ITEM_MAIN, weather.main)
                    }
                }
                startActivity(intent)
            }

        }
        recyclerView.adapter = adapter

        viewModel.weathers?.observe(this, Observer<PagedList<WeatherItem>> { list ->
            adapter.submitList(list)
        })

        viewModel.networkState?.observe(this, Observer<NetworkState> { networkState ->
            networkState?.let {
                //                swipeRefresh.isRefreshing = it.status == NetworkState.Status.RUNNING
            }
        })
    }

    override fun onReloadClick() {
        Toast.makeText(this, "Reload fragment clicked!", Toast.LENGTH_SHORT).show()
        viewModel.refreshData()
    }

}
