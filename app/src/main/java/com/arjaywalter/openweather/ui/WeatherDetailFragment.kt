package com.arjaywalter.openweather.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.arjaywalter.openweather.Injection
import com.arjaywalter.openweather.R
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.toIconUrl
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_weather_detail.*
import kotlinx.android.synthetic.main.weather_detail.*

/**
 * A fragment representing a single Weather detail screen.
 * This fragment is either contained in a [WeatherListActivity]
 * in two-pane mode (on tablets) or a [WeatherDetailActivity]
 * on handsets.
 */
class WeatherDetailFragment : Fragment() {

    private lateinit var viewModel: WeatherDetailViewModel

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.action_refresh -> {
                viewModel.loadWeather()
                return true
            }
        }
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        var weatherId = 0
        arguments?.let {
            if (it.containsKey(ARG_ITEM_ID)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                weatherId = it.getInt(ARG_ITEM_ID)
            }
            if (it.containsKey(ARG_ITEM_NAME)) {
                val name = it.getString(ARG_ITEM_NAME)
              setToolbarTitle(name)
            }
        }

        // Get the ViewModel.
        viewModel = ViewModelProviders.of(this, Injection.provideDetailViewModelFactory(requireContext(), weatherId))
            .get(WeatherDetailViewModel::class.java)

        viewModel.getWeather().observe(this, Observer<WeatherItem> { item ->
            item?.let {

               setToolbarTitle( it.name)

                it.weather?.let { it1 ->
                    val w = it1[0]
                    setWeatherText(w.main)

                    setIconImage(w.icon?.toIconUrl())
                }
            }

        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {

            if (it.containsKey(ARG_ITEM_TEMP)) {
                val tempS = it.getString(ARG_ITEM_TEMP)
                setTempText(tempS)
            }

            if (it.containsKey(ARG_ITEM_MAIN)) {
                val main = it.getString(ARG_ITEM_MAIN)
                setWeatherText(main)
            }

            if (it.containsKey(ARG_ITEM_ICON)) {
                val icon = it.getString(ARG_ITEM_ICON)
                setIconImage(icon)

            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.weather_detail, container, false)
    }

    private fun setToolbarTitle(name: String?) {
        activity?.toolbar_layout?.title = name
    }


    private fun setWeatherText(text: String?) {
        weather.text = text

    }

    private fun setTempText(tempS: String?) {
        temp.text = getString(R.string.temperature_, tempS)
    }

    private fun setIconImage(icon: String?) {
        Glide.with(this)
            .load(icon)
            .into(imageView)
    }

    companion object {
        /**
         * The fragment argument representing the item ID that this fragment
         * represents.
         */
        const val ARG_ITEM_ID = "item_id"
        const val ARG_ITEM_NAME = "item_name"
        const val ARG_ITEM_TEMP = "item_temp"
        const val ARG_ITEM_MAIN = "item_main"
        const val ARG_ITEM_ICON = "item_icon"
    }
}
