package com.arjaywalter.openweather.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.arjaywalter.openweather.R
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.toDegreeCelsius
import com.arjaywalter.openweather.toIconUrl
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_weather.view.*


class WeatherListAdapter(private val clickListener: (WeatherItem) -> Unit) :
    PagedListAdapter<WeatherItem, WeatherListAdapter.ViewHolder>(DIFF_CALLBACK) {

    var requestOptions = RequestOptions()

    init {
        requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(24))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.item_weather, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, clickListener, requestOptions) }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: WeatherItem, clickListener: (WeatherItem) -> Unit, requestOptions: RequestOptions) {

            itemView.location.text = item.name
            item.weather?.let {
                val weather = it[0]
                itemView.weather.text = weather.main

//                Glide.with(itemView.context)
//                    .load(weather.icon?.toIconUrl())
//                    .apply(requestOptions)
//                    .into(itemView.imageView)

            }
            itemView.temp.text = itemView.context.getString(R.string.temperature_, item.main?.temp?.toDegreeCelsius())

            itemView.setOnClickListener { clickListener(item) }
        }
    }


    companion object {
        private val DIFF_CALLBACK = object :
            DiffUtil.ItemCallback<WeatherItem>() {
            // WeatherItem details may have changed if reloaded from the database,
            // but ID is fixed.
            override fun areItemsTheSame(
                oldItem: WeatherItem,
                newItem: WeatherItem
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: WeatherItem,
                newItem: WeatherItem
            ): Boolean = oldItem == newItem
        }
    }
}