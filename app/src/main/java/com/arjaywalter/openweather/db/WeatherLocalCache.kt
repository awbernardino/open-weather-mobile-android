/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arjaywalter.openweather.db

import android.arch.lifecycle.LiveData
import android.arch.paging.PagedList
import android.util.Log
import com.arjaywalter.openweather.model.WeatherItem
import java.util.concurrent.Executor

/**
 * Class that handles the DAO local data source. This ensures that methods are triggered on the
 * correct executor.
 */
class WeatherLocalCache(
    private val dao: WeatherDao,
    private val ioExecutor: Executor) {

    /**
     * Insert a list of list in the database, on a background thread.
     */
    fun insert(list: List<WeatherItem>, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Log.d("WeatherLocalCache", "inserting ${list.size} list")
            dao.insert(list)
            insertFinished()
        }
    }


    fun insert(item: WeatherItem, insertFinished: () -> Unit) {
        ioExecutor.execute {
            Log.d("WeatherLocalCache", "inserting item ${item.name}")
            dao.insert(item)
            insertFinished()
        }
    }


    fun liveDataList(): LiveData<PagedList<WeatherItem>> {
        return dao.getWeathers()as LiveData<PagedList<WeatherItem>>
    }

    fun liveData(id: Int): LiveData<WeatherItem> {
        return dao.getWeatherById(id)
    }
}