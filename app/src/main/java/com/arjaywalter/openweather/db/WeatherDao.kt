package com.arjaywalter.openweather.db

import android.arch.lifecycle.LiveData
import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.arjaywalter.openweather.model.WeatherItem

@Dao
interface WeatherDao {

    @Query("SELECT * FROM weathers ORDER BY name ASC")
    fun getWeathers(): LiveData<List<WeatherItem>>

    // The Int type parameter tells Room to use a PositionalDataSource
    // object, with position-based loading under the hood.
    @Query("SELECT * FROM weathers")
    fun weathers(): DataSource.Factory<Int, WeatherItem>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: List<WeatherItem>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: WeatherItem)

    @Query("SELECT * FROM weathers WHERE name LIKE :queryString")
    fun weathersByTitle(queryString: String): LiveData<List<WeatherItem>>


    @Query("SELECT * FROM weathers WHERE id =:id")
    fun getWeatherById(id: Int): LiveData<WeatherItem>

    @Query("DELETE FROM weathers")
    fun deleteAll()

    @Query("SELECT * FROM weathers ORDER BY name DESC")
    fun getWeathersMutable(): MutableList<WeatherItem>
}
