package com.arjaywalter.openweather.model

data class WeatherResponse(
    val cnt: Int,
    val list: List<WeatherItem>
) {
}