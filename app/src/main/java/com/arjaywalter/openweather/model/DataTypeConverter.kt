package com.arjaywalter.openweather.model

import android.arch.persistence.room.TypeConverter
import com.google.gson.reflect.TypeToken
import java.util.Collections.emptyList
import com.google.gson.Gson
import java.util.*


class DataTypeConverter {
    private val gson = Gson()
    @TypeConverter
    fun stringToList(data: String?): List<WeatherItem.Weather> {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<WeatherItem.Weather>>() {

        }.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun ListToString(someObjects: List<WeatherItem.Weather>): String {
        return gson.toJson(someObjects)
    }
}
