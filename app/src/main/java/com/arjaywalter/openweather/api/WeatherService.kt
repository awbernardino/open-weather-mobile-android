/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arjaywalter.openweather.api

import android.util.Log
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.model.WeatherResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

private const val TAG = "WeatherService"
private const val IN_QUALIFIER = "in:name,description"


fun getWeathersData(
    service: WeatherService,
    page: Int,
    itemsPerPage: Int,
    onSuccess: (list: List<WeatherItem>) -> Unit,
    onError: (error: String) -> Unit
) {
    Log.d(TAG, "page: $page, itemsPerPage: $itemsPerPage")

//    val apiQuery = query + IN_QUALIFIER

    service.getWeathers().enqueue(
        object : Callback<WeatherResponse> {
            override fun onFailure(call: Call<WeatherResponse>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<WeatherResponse>?,
                response: Response<WeatherResponse>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val repos = response.body()?.list ?: emptyList()
                    onSuccess(repos)
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}

fun getWeatherById(
    id: Int,
    service: WeatherService,
    page: Int,
    itemsPerPage: Int,
    onSuccess: (item: WeatherItem) -> Unit,
    onError: (error: String) -> Unit
) {
    Log.d(TAG, "page: $page, itemsPerPage: $itemsPerPage")

//    val apiQuery = query + IN_QUALIFIER

    service.getWeatherById(id).enqueue(
        object : Callback<WeatherItem> {
            override fun onFailure(call: Call<WeatherItem>?, t: Throwable) {
                Log.d(TAG, "fail to get data")
                onError(t.message ?: "unknown error")
            }

            override fun onResponse(
                call: Call<WeatherItem>?,
                response: Response<WeatherItem>
            ) {
                Log.d(TAG, "got a response $response")
                if (response.isSuccessful) {
                    val item = response.body()
                    item?.let { onSuccess(it) }
                } else {
                    onError(response.errorBody()?.string() ?: "Unknown error")
                }
            }
        }
    )
}


/**
 * Github API communication setup via Retrofit.
 */
interface WeatherService {

    @GET("group")
    fun getWeathers(
        @Query("id") page: String = COUNTRY_IDs,
        @Query("appid") apiKey: String? = API_KEY
    ): Call<WeatherResponse>

    @GET("weather")
    fun getWeatherById(
        @Query("id") page: Int,
        @Query("appid") apiKey: String? = API_KEY
    ): Call<WeatherItem>


    companion object {
        private const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
        const val API_KEY = "c2b653562b0916bc48f9496aee691cc1"
        const val COUNTRY_IDs = "2643743,3067696,5391959"
        const val COUNTRY_IDs2 = "2643743,3067696,5391959"
        var POSTER_BASE_URL = "https://image.tmdb.org/t/p/w500"

        fun create(): WeatherService {
            val logger = HttpLoggingInterceptor()
            logger.level = Level.BASIC

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherService::class.java)
        }
    }
}