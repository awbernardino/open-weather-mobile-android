/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arjaywalter.openweather

import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import com.arjaywalter.openweather.api.WeatherService
import com.arjaywalter.openweather.data.WeatherDataFactory
import com.arjaywalter.openweather.data.WeatherDataSource
import com.arjaywalter.openweather.data.WeatherRepository
import com.arjaywalter.openweather.db.WeatherDatabase
import com.arjaywalter.openweather.db.WeatherLocalCache
import com.arjaywalter.openweather.ui.WeatherDetailViewModel
import com.arjaywalter.openweather.ui.ViewModelFactory
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Simple Class that handles object creation.
 * Can be replaced by Dependency Injection like Kotlin Koin or Dagger
 */
object Injection {

    /**
     * Creates an instance of [WeatherService]
     */
    private fun provideWeatherService(): WeatherService {
        return WeatherService.create()
    }

    /**
     * Creates an instance of [WeatherDataSource] based on the [WeatherService]
     */
    private fun provideFeedDataSource(context: Context): WeatherDataSource {
        return WeatherDataSource(provideWeatherService(), provideDatabase(context).weatherDao(), provideDbExecutor())
    }

    private fun provideDbExecutor(): Executor {
        return Executors.newSingleThreadExecutor()
    }

    private fun provideDatabase(context: Context): WeatherDatabase {
        return WeatherDatabase.getInstance(context)
    }

    /**
     * Creates an instance of [WeatherDataFactory] based on the [WeatherService]
     */
    private fun provideDataFactory(context: Context): WeatherDataFactory {
        return WeatherDataFactory(provideFeedDataSource(context))
    }

    /**
     * Creates an instance of [WeatherLocalCache] based on the database DAO.
     */
    private fun provideCache(context: Context): WeatherLocalCache {
        return WeatherLocalCache(provideDatabase(context).weatherDao(), provideDbExecutor())
    }

    /**
     * Creates an instance of [WeatherRepository] based on the [WeatherService] and a
     * [WeatherLocalCache]
     */
    private fun provideRepository(context: Context): WeatherRepository {
        return WeatherRepository(provideWeatherService(), provideCache(context))
    }

    /**
     * Provides the [ViewModelProvider.Factory] that is then used to get a reference to
     * [ViewModel] objects.
     */
    fun provideViewModelFactory(context: Context): ViewModelProvider.Factory {
        return ViewModelFactory(provideRepository(context), provideDataFactory(context))
    }

    fun provideDetailViewModelFactory(context: Context, id: Int): ViewModelProvider.Factory? {
        return WeatherDetailViewModel.Factory(provideRepository(context), id)
    }

}