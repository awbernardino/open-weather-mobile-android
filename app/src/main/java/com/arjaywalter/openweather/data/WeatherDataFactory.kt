package com.arjaywalter.openweather.data

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.arjaywalter.openweather.model.WeatherItem

class WeatherDataFactory(val dataSource: WeatherDataSource) : DataSource.Factory<Long, WeatherItem>() {

    val mutableLiveData: MutableLiveData<WeatherDataSource> = MutableLiveData()

    override fun create(): DataSource<Long, WeatherItem> {
        mutableLiveData.postValue(dataSource)
        return dataSource
    }
}