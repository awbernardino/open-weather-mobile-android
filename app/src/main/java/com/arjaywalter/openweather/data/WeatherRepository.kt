/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.arjaywalter.openweather.data

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.arjaywalter.openweather.api.WeatherService
import com.arjaywalter.openweather.api.getWeatherById
import com.arjaywalter.openweather.api.getWeathersData
import com.arjaywalter.openweather.db.WeatherLocalCache
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.model.WeatherResult


/**
 * Repository class that works with local and remote data sources.
 */
class WeatherRepository(
    private val service: WeatherService,
    private val cache: WeatherLocalCache
) {

    // keep the last requested page. When the request is successful, increment the page number.
    private var lastRequestedPage = 1

    // LiveData of weather item
    private val data = MutableLiveData<WeatherItem>()

    // LiveData of network errors.
    private val networkErrors = MutableLiveData<String>()

    // avoid triggering multiple requests in the same time
    private var isRequestInProgress = false

    fun fetchWeathers(): WeatherResult {
        lastRequestedPage = 1
        fetchAndSaveData()

        // Get data from the local cache
        val data = cache.liveDataList()

        return WeatherResult(data, networkErrors)
    }

    private fun fetchAndSaveData() {
        if (isRequestInProgress) return

        isRequestInProgress = true
        getWeathersData(service, lastRequestedPage, NETWORK_PAGE_SIZE, { items ->
            cache.insert(items) {
                lastRequestedPage++
                isRequestInProgress = false
            }
        }, { error ->
            networkErrors.postValue(error)
            isRequestInProgress = false
        })
    }

    fun fetchWeather(id: Int): MutableLiveData<WeatherItem> {

        if (isRequestInProgress) return data

        isRequestInProgress = true
        getWeatherById(id, service, lastRequestedPage, NETWORK_PAGE_SIZE, { item ->
            cache.insert(item) {
                data.postValue(item)
                isRequestInProgress = false
            }
        }, { error ->
            data.postValue(cache.liveData(id).value)
            networkErrors.postValue(error)
            isRequestInProgress = false
        })

        return data
    }


    companion object {
        private const val NETWORK_PAGE_SIZE = 50
    }
}