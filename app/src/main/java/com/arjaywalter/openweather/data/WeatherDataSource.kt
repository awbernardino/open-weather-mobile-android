package com.arjaywalter.openweather.data

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import android.util.Log
import com.arjaywalter.openweather.utils.NetworkState
import com.arjaywalter.openweather.api.WeatherService
import com.arjaywalter.openweather.db.WeatherDao
import com.arjaywalter.openweather.model.WeatherItem
import com.arjaywalter.openweather.model.WeatherResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor


class WeatherDataSource(private val service: WeatherService,
                        private val dao: WeatherDao,
                        private val ioExecutor: Executor) : PageKeyedDataSource<Long, WeatherItem>() {

    /*
     * Step 1: Initialize the restApiFactory.
     * The networkState and initialLoading variables
     * are for updating the UI when data is being fetched
     * by displaying a progress bar
     */

    val networkState: MutableLiveData<NetworkState> = MutableLiveData()
    val initialLoading: MutableLiveData<NetworkState> = MutableLiveData()

    /*
     * Step 2: This method is responsible to load the data initially
     * when app screen is launched for the first time.
     * We are fetching the first page data from the api
     * and passing it via the callback method to the UI.
     */
    override fun loadInitial(params: LoadInitialParams<Long>,
                             callback: LoadInitialCallback<Long, WeatherItem>) {

        initialLoading.postValue(NetworkState.LOADING)
        networkState.postValue(NetworkState.LOADING)

        service.getWeathers()
                .enqueue(object : Callback<WeatherResponse> {
                    override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                        if (response.isSuccessful) {

                            response.body()?.list?.let {
                                callback.onResult(it, null, 2L)
                                ioExecutor.execute {
                                    dao.deleteAll()
                                    Log.d("WeatherDataSource", "loadInitial inserting ${it.size} weathers")
                                    dao.insert(it)
                                }
                            }
                            initialLoading.postValue(NetworkState.LOADED)
                            networkState.postValue(NetworkState.LOADED)


                        } else {
                            initialLoading.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                            networkState.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
                            callback.onResult(dao.getWeathersMutable(), null, null)
                        }
                    }

                    override fun onFailure(call: Call<WeatherResponse>, t: Throwable?) {
                        Log.d("WeatherDataSource", "loadInitial onFailure")
                        var movies: List<WeatherItem>?
                        ioExecutor.execute {
                            movies = dao.getWeathersMutable()
                            Log.d("WeatherDataSource", "loadInitial onFailure ${Gson().toJson(movies)}")
                            callback.onResult(movies as MutableList<WeatherItem>, null, null)
                        }

                        val errorMessage = if (t == null) "unknown error" else t.message
                        networkState.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage!!))
                    }
                })
    }


    override fun loadBefore(params: LoadParams<Long>,
                            callback: LoadCallback<Long, WeatherItem>) {

    }


    /*
     * Step 3: This method it is responsible for the subsequent call to load the data page wise.
     * This method is executed in the background thread
     * We are fetching the next page data from the api
     * and passing it via the callback method to the UI.
     * The "params.key" variable will have the updated value.
     */
    override fun loadAfter(params: LoadParams<Long>,
                           callback: LoadCallback<Long, WeatherItem>) {

        // No need to load more we have a fixed 3 country weather

       /* Log.i(TAG, "Loading Rang " + params.key + " Count " + params.requestedLoadSize)

        networkState.postValue(NetworkState.LOADING)

        service.getWeathers().enqueue(object : Callback<WeatherResponse> {
            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                *//*
                 * If the request is successful, then we will update the callback
                 * with the latest feed items and
                 * "params.key+1" -> set the next key for the next iteration.
                 *//*
                if (response.isSuccessful) {
                    val nextKey = if (params.key == response.body()?.cnt?.toLong()) null else params.key + 1
                    response.body()?.list?.let {
                        callback.onResult(it, nextKey)
                        ioExecutor.execute {
                            Log.d("WeatherDataSource", "loadAfter inserting ${it.size} weathers")
                            dao.insert(it)
                        }
                    }
                    networkState.postValue(NetworkState.LOADED)

                } else
                    networkState.postValue(NetworkState(NetworkState.Status.FAILED, response.message()))
            }

            override fun onFailure(call: Call<WeatherResponse>, t: Throwable?) {
                Log.d(TAG, "loadAfter onFailure")
                val errorMessage = if (t == null) "unknown error" else t.message
                networkState.postValue(NetworkState(NetworkState.Status.FAILED, errorMessage!!))
            }
        })*/
    }

    companion object {

        private val TAG = WeatherDataSource::class.java.simpleName
    }
}
